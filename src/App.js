import React, { Component } from 'react';
import '../libs/bootstrap.min.css';
import '../libs/react-bootstrap-table-all.min.css';
import './App.css';
import {FormGroup, FormControl, Alert} from 'react-bootstrap';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import LoadingOverlay from 'react-loading-overlay';

/** 
 * App.js allows the user to search with a search bar in the Marvel database,
 * and then displays the results in a nicely-formatted table.
 * See here for updates on the PropTypes warning currently shown in the console
 * https://github.com/react-toolbox/react-toolbox/pull/1413
 **/

import axios from 'axios';

//TODO Change this to actual production server url and port
var baseUrl = 'http://localhost:3000/search';

class App extends Component {
  
  constructor(){
    super();
    this.state = {
      items:[],
      count:0,
      message:"",
      itemsPerPage:10,
      pageStart:0,
      sortOrder:'asc',
      searchText:"",
      searching:false
    };
    
    //I was really concerned about async search queries stacking on top of each
    //other out of order, so this is my intended solution
    this.lastSearch = 0;
    this.cancelToken = null;
    
    this.search = this.search.bind(this);
    this.clearResults = this.clearResults.bind(this);
    this.handleFailedRequest = this.handleFailedRequest.bind(this);
  }
  
 /**
  * Wipes the current search results from the table.
  * @param {string} message - Optionally sets an error message if the server returned one
  */
  clearResults(message = ""){
    this.lastSearch = (+ new Date());
    this.setState({
      items:[],
      count:0,
      message:message,
      pageStart:0,
      searching:false
    });
  }
  
  /**
  * Clears the table results while also enabling the error alert
  */
  handleFailedRequest(){
      this.clearResults("Could not connect to server. Please check your internet connection");
  }
  
  /**
  * Executes a search on the server using the provided state parameters
  */
  search(){
      
    //Get rid of the old request if it's still running
    if(this.cancelToken){
        this.cancelToken.cancel();
    }
    this.cancelToken = App.axios.CancelToken.source();
    var that = this;
    var searchText = this.state.searchText;
    var sortOrder = this.state.sortOrder;
    var itemsPerPage = this.state.itemsPerPage;
    var pageStart = this.state.pageStart*itemsPerPage;
    
    if(!searchText){
        //TODO Check that the backend handles this nicely
        //Wipe the results
        this.clearResults();
        return;
    }
    this.setState({searching:true});
    
    //TODO Fix this IP Address
    var url = `${baseUrl}?text=${searchText}&start=${pageStart}&count=${itemsPerPage}&sort=${sortOrder}&timeStamp=${+new Date()}`;
    
    App.axios.get(url,{cancelToken:this.cancelToken.token,timeout:5000}).then(function(resp){
        if(resp && resp.status ===200){
            try{               
                //Make sure it is the latest search
                if(that.lastSearch<=resp.data.timeStamp){
                    that.lastSearch = resp.data.timeStamp;
                    that.setState({
                        items:resp.data.results,
                        count:resp.data.count,
                        message:"",
                        searching:false
                    });
                }
            }
            catch(err){
                that.handleFailedRequest();
            }
        }
        else{
            that.handleFailedRequest();
        }
    }).catch(function(err){
        if(!App.axios.isCancel(err)){
            that.handleFailedRequest();
            console.log(err);
        }
        else{
            console.log("Request was cancelled");
        }
    });
  }
  
  /**
  * Renders everything, including the search bar, search results, and header and footer
  */
  render() {
    var that = this;
    return (
      <div id='page'>
        <div id='pageContent'>
        <h2> Marvel Search </h2>
        <form>
          <FormGroup>
            <FormControl id="searchBar"
                         maxLength="30"
                         type="text" 
                         placeholder="Enter Marvel character"
                         autoComplete="off"
                         onKeyPress={(event)=>{
                             //Don't let enter reload the whole page
                             if(event.which === 13){
                                event.preventDefault();
                             }
                         }
                         }
                         onKeyUp={(searchEvent)=>{
                             this.setState(
                             {searchText:searchEvent.target.value,
                             pageStart:0},
                             this.search);
                         }
                         }/>
          </FormGroup>
        </form>
        {
            function(){
                //Had to wrap this in a function to avoid weird coverage library errors again
                return that.state.message ? <Alert bsStyle="danger" onDismiss={()=>{
                    that.setState({message:""})
                }}><p> {that.state.message} </p> </Alert>: null;
            }()
        }
        <LoadingOverlay active={this.state.searching}
                        className='loadingOverlay'
                        spinner
                        text='Loading...'>
            <div id='marvelResultsTable'>
            <BootstrapTable data={this.state.items}
                            remote={true}
                            pagination={true}
                            fetchInfo={{dataTotalSize:this.state.count}}
                            options={{sizePerPage:this.state.itemsPerPage,
                                      onPageChange: (page,sizePerPage)=>{
                                          this.setState({
                                              pageStart:page-1
                                          },this.search);
                                      },
                                      sizePerPageList: [this.state.itemsPerPage],
                                      page:this.state.pageStart+1,
                                      }}>
                <TableHeaderColumn dataField='id' isKey={true}> # </TableHeaderColumn>
                <TableHeaderColumn dataField='name' > Character </TableHeaderColumn>
                <TableHeaderColumn dataField='picture' dataFormat={(cell,row)=>{
                    return <a target="_blank" href={row.link}> 
                            <img alt='Not found' src={cell}/>
                           </a>;
                }
                }> Picture </TableHeaderColumn>
            </BootstrapTable>  
            </div>
        </LoadingOverlay>
        </div>
        <div className='navbar navbar-bottom'>
          Data provided by Marvel. © 2014 Marvel
        </div>
      </div>
    );
  }
}

export default App;

//Normally I like to mock using rewire but it is breaking my coverage
//library so I will just expose axios here globally
App.axios = axios;