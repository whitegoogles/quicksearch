require('../scripts/dom.js');
require('should');
require('../test/Mocks.js');

process.env.NODE_ENV = 'test';
process.env.PUBLIC_URL = '';

// Load environment variables from .env file. Suppress warnings using silent
// if this file is missing. dotenv will never modify any environment variables
// that have already been set.
// https://github.com/motdotla/dotenv
require('dotenv').config({silent: true});

require('babel-register')({
    ignore: function(filename){
        var ignored = filename.indexOf("node_modules")>=0;
        return ignored;
    }
});
//var babel = require('babel-core');
function noop(){
    return null;
}
require.extensions['.svg'] = noop;
require.extensions['.css'] = noop;
require.extensions['.scss'] = noop;
require.extensions['.ttf'] = noop;

global.navigator = {
  userAgent: 'node.js'
};