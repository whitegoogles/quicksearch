require('./test.js');
var istanbul = require('babel-istanbul');
var testDir = 'test';
var coverDir = 'coverage';
var Mocha = require('mocha');
var fs = require('fs-extra');
var path = require('path');

if(!fs.existsSync(coverDir)){
    fs.mkdirSync(coverDir);
}

function instrument(){
    var instrumenter = new istanbul.Instrumenter({
        coverageVariable: "__coverage__",
        preserveComments: false,
        compact: true,
        esModules: true,
        autoWrap: false,
        produceSourceMap: false,
        sourceMapUrlCallback: null,
        debug: false
    });
    fs.copySync(testDir,coverDir);
    fs.readdirSync(coverDir).filter(function(fileName){
        return (!fileName.endsWith('.test.js') && fileName.endsWith('.js'));
    }).forEach(function(fileName){
        var instrumented = fs.readFileSync(path.join(coverDir,fileName),'utf8');
        instrumented = instrumenter.instrumentSync(instrumented,path.join(testDir,fileName));
        fs.writeFileSync(path.join(coverDir,fileName),instrumented);
    });
}

function report(){
    var Report = istanbul.Report,
        Collector = istanbul.Collector,
        reporters = ['html','text-summary'],
        collector = new Collector();
    collector.add(__coverage__);
    reporters.forEach(function(reporter){
        Report.create(reporter,{
            dir:coverDir+'/'+reporter
        }).writeReport(collector,true);
    });
}

var mocha = new Mocha();

instrument();
fs.readdirSync(coverDir).filter(function(file){
    return file.endsWith('.test.js');
}).forEach(function(file){
    var testCode = fs.readFileSync(path.join(coverDir,file),'utf8');   
    fs.writeFileSync(path.join(coverDir,file),testCode);
    mocha.addFile(path.join(coverDir,file));
});

mocha.run(function(failures){
    if(failures){
        process.exit(1);
    }
    process.on('exit',function(){
       report();       
    });
});
