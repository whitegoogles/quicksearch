
function mockAxios(App){
    var storeAxios = App.axios;
    var retObj = {
        mock:null,
        restore: null,
        then: ()=>{},
        catch: ()=>{}
    }
    var axiosMock = {
        CancelToken:{
            source:()=>{
                return {token:'',cancel:()=>{}};
            }
        },
        get: ()=>{
            return {
                then: (success)=>{
                    retObj.then(success);
                    return {
                        'catch': (error)=>{
                            retObj.catch(error);
                        }
                    }
                }
            }
        },
        isCancel:()=>{}
    };
    App.axios = axiosMock;
    retObj.mock = axiosMock;
    retObj.restore = ()=>{
        App.axios = storeAxios;
    }
    return retObj;   
}

global.Mocks = {
    mockAxios:mockAxios
};
