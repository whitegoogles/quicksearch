import App from '../src/App';
var noop = ()=>{};

//TODO Make this a better description
describe("App unit tests",()=>{
    var app;
    var setStateMock;
    beforeEach(()=>{
        app = new App();
        setStateMock = noop;
        app.setState = (newState)=>{
            setStateMock(newState);
        }
    });
    describe("Clearing search results",()=>{
        it('should set the timestamp to the current time when wiping results', () => {
            //This will ensure that pending searches will not overwrite
            //search contents even if the cancelling of these searches fails
            var prevSearchTime = app.lastSearch;
            app.clearResults();
            app.lastSearch.should.be.greaterThan(prevSearchTime);
        });
        it('should clear the items, count, pagestart, and searching states',(done)=>{
            app.state.items = "josh";
            app.state.count = 3000;
            app.state.pageStart = 12;
            app.state.searching = true;
            setStateMock = (newState)=>{
                JSON.stringify(newState.items).should.be.exactly('[]');
                newState.count.should.be.exactly(0);
                newState.pageStart.should.be.exactly(0);
                newState.searching.should.be.exactly(false);
                done();
            };
            app.clearResults();
        });
        it('should set the message state to the passed in message',(done)=>{
            app.state.message = "well, i'm not the right message";
            var testMsg = "Marvel Defenders";
            setStateMock = (newState)=>{
                newState.message.should.be.exactly(testMsg);
                done();
            }
            app.clearResults(testMsg);
        });
    });
    describe("Handling failed requests",()=>{
        it("should call clear results with a specific message",(done)=>{
            app.clearResults = (msg)=>{
                msg.should.be.not.empty();
                done();
            }
            app.handleFailedRequest();
        });
    });
    describe("Searching",()=>{
        var axiosMock;
        
        beforeEach(()=>{
            
            //Unfortunately, istanbul was struggling to get coverage because
            //of the axios mocking library I was using, so I had to just write
            //some ugly manual mocking code here.
            axiosMock = Mocks.mockAxios(App);
        });
        afterEach(()=>{
            axiosMock.restore();
        });
        describe("bailing on a search with no search text",()=>{
            it("should cancel a previous axios request if there is one",(done)=>{
                app.cancelToken = {
                    cancel: done
                }
                app.search();
            });
            it('should clear results if the search text is blank',(done)=>{
                app.clearResults = done;
                app.search();
            });
            it('should return if the search text is blank',(done)=>{
                app.clearResults = noop;
                app.setState = done;
                app.search();
                done();
            });
        });
        describe('actually searching with valid search text',()=>{
            it('should set searching to be true',(done)=>{
                app.state.searchText = "Punisher";
                setStateMock = (newState)=>{
                    newState.searching.should.be.true();
                    done();
                }
                app.search();
            });
            function testHandleFailedResponse(done){
                app.handleFailedRequest = done;
                app.state.searchText = "Daredevil"
                app.search();
            }                
            it('should respond to an empty server response by calling handle failed request',(done)=>{
                axiosMock.then = (success)=>success(null);
                testHandleFailedResponse(done);
            });
            it('should respond to a non-200 server response by calling handle failed request',(done)=>{
                axiosMock.then = (success)=>success({status:301});
                testHandleFailedResponse(done);
            });
            it('should respond to a malformed server response by calling handle failed request',(done)=>{
                axiosMock.then = (success)=>success({status:200});
                testHandleFailedResponse(done);
            });
            it('should ignore an old search',(done)=>{
                app.lastSearch = 2;
                app.state.searchText = 'Black Widow';
                app.handleFailedRequest = done;
                axiosMock.then = (success)=>{
                    setStateMock = done;
                    success({status:200,data:{timeStamp:1}});               
                };
                app.search();
                done();
            });
            /*it('should respond to a server error by calling handle failed request',(done)=>{
                axiosMock.catch = (error)=>{
                    error("default error");
                }
                app.state.searchText = 'Thor';
                app.handleFailedRequest = done;
                app.search();
            });
            it('should respond to a cancel server error by doing nothing',(done)=>{
                axiosMock.catch = (error)=>{
                    setStateMock = done;
                    //error("default error");
                }
                app.state.searchText = "Hawkeye";
                axiosMock.isCancel = ()=>true;
                app.handleFailedRequest = done;
                app.search();
                done();
            });*/
            it('should load a successful server response into state',(done)=>{
                var timeStamp = 1;
                var count = 3;
                var results = {test:'123'};
                axiosMock.then = (success)=>{
                    setStateMock = (newState)=>{
                        JSON.stringify(newState.items).should.be.exactly(JSON.stringify(results));
                        newState.count.should.be.exactly(count);
                        newState.message.should.be.empty();
                        newState.searching.should.not.be.true();
                        done();
                    }
                    success({status:200,data:{timeStamp,count,results}});
                }
                app.state.searchText = "Loki";
                app.search();
            });
        });
    });
});

