import React from 'react';
import ReactTestUtils from 'react-addons-test-utils';
import {shallow,mount} from 'enzyme';
var fs = require('fs');
var noop = ()=>{};

import App from '../src/App';

var daredevilSearch = JSON.parse(fs.readFileSync('test/daredevilSearch.json'));
var bSearch = JSON.parse(fs.readFileSync('test/bSearch.json'));

//TODO Make this a better description
describe("App integration tests",()=>{
    var mountedApp;
    var axiosMock;
    beforeEach(()=>{
        mountedApp = mount(<App/>);
        axiosMock = Mocks.mockAxios(App); 
    });
    afterEach(()=>{
        if(mountedApp){
            mountedApp.unmount();
        }
        axiosMock.restore();
    });
    var tableHeadings = ["#","Character","Picture"];
    var tableId = '#marvelResultsTable';
    
    //Most of these table checking functions are copied from another project
    //as I have found these really useful several times. At some point I might
    //move this stuff into some test utils npm package that I can use universally
    function checkTable(expectedTable){       
        //1 extra for the header
        var tableRows = mountedApp.find(tableId+" tr").nodes;
        (tableRows.length).should.be.exactly(expectedTable.length+1);
        var i;
        for(i = 0; i<expectedTable.length; i+=1){
            var actualRow = tableRows[i+1];
            checkRow(expectedTable[i],actualRow);
        }
    }
    
    function preprocessSearch(searchResults){
        var checkResults = [];
        searchResults.results.forEach((row)=>{
            var checkRow = [];
            checkRow['#'] = (actualCell)=> checkCell(actualCell,row.id);
            checkRow.Character = (actualCell)=> checkCell(actualCell,row.name);
            checkRow.Picture = (actualCell)=>{
                actualCell.querySelector('a').href.should.be.exactly(row.link);
                actualCell.querySelector('img').src.should.be.exactly(row.picture);
            }
            checkResults.push(checkRow);
        });
        return checkResults;
    }
    
    function checkRow(expectedRow,actualRow){
        var rowLength = Object.keys(expectedRow).length;
        (actualRow.querySelectorAll('td').length).should.be.exactly(rowLength);
        var i;
        for(i = 0; i<rowLength; i+=1){
            var actualCell = actualRow.querySelectorAll('td')[i];
            expectedRow[Object.keys(expectedRow)[i]](actualCell);
        }
    }
    function checkCell(actualCell,expectedCell){
        (""+expectedCell).should.be.exactly(actualCell.textContent);
    }
    
    function checkTableHeadings(expectedHeadings){
        var tableHeadings = mountedApp.find(tableId +" th").nodes;
        (tableHeadings.length).should.be.exactly(expectedHeadings.length);
        let spot = 0;
        expectedHeadings.forEach(function(col){
            (tableHeadings[spot].textContent.trim()).should.be.exactly(col.trim());
            spot+=1;
        });
    }
    
    function checkEmptyTable(){
        checkTableHeadings(tableHeadings);
        mountedApp.find(tableId+" tbody td").nodes.length.should.be.exactly(1);
    }
    
    function search(value){
        var searchInput = mountedApp.find('#searchBar').node;
        ReactTestUtils.Simulate.focus(searchInput);
        ReactTestUtils.Simulate.keyUp(searchInput,{target:{value:value}});
    }
    
    function checkPage(page){
        mountedApp.find('.pagination .active').node.title.should.be.exactly(""+page);
    }
    
    function movePage(page){
        var pageNode = mountedApp.find('.pagination a').nodes[page-1];
        ReactTestUtils.Simulate.click(pageNode);
    }
    
    function closeAlert(){
        var alertNode = mountedApp.find('.alert .close').node;
        ReactTestUtils.Simulate.click(alertNode);
    }
    
    function successfulSearch(){
        axiosMock.then = (cb)=>{
            cb({
                status:200,
                data:bSearch
            });
        }
        search('b');
    }

    describe("On startup",()=>{
        it('should display the searchbar on startup',()=>{
            (mountedApp.find('#searchBar').nodes.length).should.be.exactly(1);
        });
        it('should display an empty table on startup',()=>{
            checkEmptyTable();
        });
    });  
    describe("while searching",()=>{
        it("should show the loading indicator",()=>{
            axiosMock.get = ()=>{};
            mountedApp.find('.loadingOverlay svg').nodes.length.should.be.exactly(0);
            search('Vision');
            mountedApp.find('.loadingOverlay svg').nodes.length.should.be.exactly(1);
        });
        it("should keep the old search results",()=>{
            axiosMock.then = (cb)=>{
                cb({
                    status:200,
                    data:daredevilSearch
                });
            }
            search('Dare');
            checkTable(preprocessSearch(daredevilSearch));
            axiosMock.then = ()=>{};
            search('j');
            checkTable(preprocessSearch(daredevilSearch));
        });
        it("should allow the user to change the search",(done)=>{
            axiosMock.then = (cb)=>{
                cb({
                    status:200,
                    data:daredevilSearch
                });
            }
            search('d');
            axiosMock.then = ()=>done();
            search('a');
        });
    })
    describe("On a successful search",()=>{
        
        beforeEach(()=>{
            successfulSearch();
        });
        it("should display the search results",()=>{
            checkTable(preprocessSearch(bSearch));
        });
        it("should reset the page",()=>{
            checkTable(preprocessSearch(bSearch));
            var initialSearch = JSON.parse(JSON.stringify(daredevilSearch));
            initialSearch.count = 50;
            axiosMock.then = (cb)=>{
                cb({
                    status:200,
                    data:initialSearch
                });
            };
            initialSearch.timeStamp = (+ new Date);
            movePage(2);
            checkTable(preprocessSearch(initialSearch));
            checkPage(2);
            search('scarlet witch');
            checkPage(1);
        });
        it("should hide the loading indicator",()=>{
            mountedApp.find('.loadingOverlay svg').nodes.length.should.be.exactly(0);
        });
    });
    describe("On an errored search",()=>{
        beforeEach(()=>{
            axiosMock.catch = (cb)=>{
                cb();
            };
            search('Thor');
        });
        it("should display the error",()=>{
            mountedApp.find('.alert-danger').nodes.length.should.be.exactly(1);
        });
        it("should allow for closing the error",()=>{
            closeAlert();
            mountedApp.find('.alert-danger').nodes.length.should.be.exactly(0);
        });
    });
    describe("On changing the table page",()=>{
        it("should show the correct new results and page on search completion",()=>{
            successfulSearch();
            checkTable(preprocessSearch(bSearch));
            var initialSearch = JSON.parse(JSON.stringify(daredevilSearch));
            initialSearch.count = 50;
            axiosMock.then = (cb)=>{
                cb({
                    status:200,
                    data:initialSearch
                });
            };
            initialSearch.timeStamp = (+ new Date);
            movePage(2);
            checkTable(preprocessSearch(initialSearch));
        });
    });
});

